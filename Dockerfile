FROM ubuntu:bionic AS homplexity-build
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:hvr/ghc
RUN apt-get update -y
ENV HC=ghc-8.4.4
RUN apt-get install -y cabal-install ${HC} alex happy
ENV HCPKG=ghc-pkg-8.4.4
RUN mkdir -p $HOME/.local/bin
ENV PATH=/opt/ghc/bin:/opt/ghc-ppa-tools/bin:$HOME/local/bin:$PATH
RUN mkdir -p /build;
ADD . /build
WORKDIR /build
RUN sed --in-place 's/-- STATIC: //' homplexity.cabal
RUN rm -rf dist-newstyle dist
RUN cabal sandbox init
RUN cabal update
RUN cabal install --only-dependencies
RUN cabal configure
RUN cabal install --bindir=/build/static --libexecdir=/build/static --reinstall
RUN ls -alth /build/static

FROM scratch AS homplexity
COPY --from=homplexity-build /build/static/homplexity-cli /homplexity
ENTRYPOINT ["/homplexity"]

# For Debian/Ubuntu/Mint
RUN curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
 
# For Debian/Ubuntu/Mint
RUN sudo apt-get install gitlab-runner
 
# for DEB based systems
RUN apt-cache madison gitlab-runner
RUN sudo apt-get install gitlab-runner=10.0.0
 
RUN --rm -t -i -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image alpine:3 \
  --url "https://gitlab.com/" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --description "docker-runner" \
  --tag-list "docker,aws" \
  --run-untagged \
  --locked="false"

